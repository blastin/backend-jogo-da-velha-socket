cmake_minimum_required(VERSION 3.12)
project(backend_socket_jogodavelha)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-g -pthread -O0 -march=native")

set(RAIZ src)

set(CLIENTE_SOCKET_PASTA ${RAIZ}/cliente_socket)
set(CLIENTE_SOCKET_FONTE ${CLIENTE_SOCKET_PASTA}/ClienteSocket.cpp)
set(CLIENTE_SOCKET_INCLUSAO ${CLIENTE_SOCKET_PASTA}/ClienteSocket.h)
set(CLIENTE_SOCKET ${CLIENTE_SOCKET_FONTE} ${CLIENTE_SOCKET_INCLUSAO})

set(CLIENTE_PASTA ${RAIZ}/cliente)
set(CLIENTE_FONTE ${CLIENTE_PASTA}/Cliente.cpp)
set(CLIENTE_INCLUSAO ${CLIENTE_PASTA}/Cliente.h ${CLIENTE_PASTA}/ICliente.h ${CLIENTE_PASTA}/IClienteComunicacao.h)
set(CLIENTE ${CLIENTE_FONTE} ${CLIENTE_INCLUSAO})

set(COMUM_PASTA ${RAIZ}/comum)
set(COMUM_FONTE ${COMUM_PASTA}/Descritor.cpp ${COMUM_PASTA}/Porta.cpp)
set(COMUM_INCLUSAO ${COMUM_PASTA}/Descritor.h ${COMUM_PASTA}/Porta.h ${COMUM_PASTA}/Identificador.h src/comum/Protocolos.h)
set(COMUM ${COMUM_FONTE} ${COMUM_INCLUSAO})

set(SALA_PASTA ${RAIZ}/sala)
set(SALA_FONTE ${SALA_PASTA}/Sala.cpp ${SALA_PASTA}/SalaID.cpp ${SALA_PASTA}/Salas.cpp)
set(SALA_INCLUSAO ${SALA_PASTA}/ISala.h ${SALA_PASTA}/ISalaCliente.h ${SALA_PASTA}/ISalas.h ${SALA_PASTA}/SalaID.h ${SALA_PASTA}/Salas.h)
set(SALA ${SALA_FONTE} ${SALA_INCLUSAO})

set(SERVER_PASTA ${RAIZ}/server)
set(SERVER_FONTE ${SERVER_PASTA}/ServerSocket.cpp)
set(SERVER_INCLUSAO ${SERVER_PASTA}/ServerSocket.h)
set(SERVER ${SERVER_FONTE} ${SERVER_INCLUSAO})

set(THREAD_PASTA ${RAIZ}/thread)
set(THREAD_FONTE ${THREAD_PASTA}/ClienteThread.cpp)
set(THREAD_INCLUSAO ${THREAD_PASTA}/ClienteThread.h)
set(THREAD ${THREAD_FONTE} ${THREAD_INCLUSAO})

set(MENSAGEM_PASTA ${RAIZ}/mensagem)
set(MENSAGEM_FONTE ${MENSAGEM_PASTA}/MensagemComum.cpp ${MENSAGEM_PASTA}/ProtocoloComunicativo.cpp ${MENSAGEM_PASTA}/ProtocoloConstrutor.cpp)
set(MENSAGEM_INCLUSAO ${MENSAGEM_PASTA}/IMensagem.h ${MENSAGEM_PASTA}/MensagemComum.h ${MENSAGEM_PASTA}/ProtocoloComunicativo.h ${MENSAGEM_PASTA}/ProtocoloConstrutor.h)
set(MENSAGEM ${MENSAGEM_INCLUSAO} ${MENSAGEM_FONTE})

set(JSON_PASTA ${RAIZ}/json_include)
set(JSON_FONTE ${JSON_PASTA}/json.hpp)
set(JSON ${JSON_FONTE})

add_executable(backend_socket_jogodavelha ${RAIZ}/main.cpp ${COMUM} ${SALA} ${SERVER} ${THREAD} ${CLIENTE_SOCKET} ${JSON} ${CLIENTE} ${MENSAGEM})