//
// Created by jefferson on 15/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_SERVERSOCKET_H
#define BACKEND_SOCKET_JOGODAVELHA_SERVERSOCKET_H

#include "../comum/Descritor.h"
#include "../comum/Porta.h"
#include "../cliente/IClienteComunicacao.h"


class ServerSocket {

private:
    const unsigned  MAXIMA_QUANTIDADE_DE_CONEXOES = 100;
    const Porta &porta;
    Descritor descritor;
    struct sockaddr_in endereco{};
    bool conectado;

    inline static sockaddr &cast_socketaddr(sockaddr_in &sockaddrIn);

    void static manipulando_sinal_sistema_operacional(int sinal);

public:

    explicit ServerSocket(Porta &porta);

    ~ServerSocket();

    void iniciarServidor();

    bool servidorConectado();

    IClienteComunicacao* novoCLienteSocket();

    void desconectarServidor();
};

#endif //BACKEND_SOCKET_JOGODAVELHA_SERVERSOCKET_H
