//
// Created by jefferson on 15/09/18.
//

#include <csignal>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstring>
#include <iostream>
#include "ServerSocket.h"
#include "../cliente_socket/ClienteSocket.h"


ServerSocket::ServerSocket(Porta &porta) : porta(porta),
                                           descritor(Descritor(socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))) {
    endereco.sin_family = AF_INET;
    endereco.sin_addr.s_addr = htonl(INADDR_ANY);
    endereco.sin_port = htons(this->porta());
    conectado = false;
}

ServerSocket::~ServerSocket() {
    close(descritor.paraPrimitivoInt());
    std::cout << "Servidor::Desconectado" << std::endl;
}

bool ServerSocket::servidorConectado() {
    return conectado;
}

IClienteComunicacao *ServerSocket::novoCLienteSocket() {
    struct sockaddr_in enderecoCLiente{};
    socklen_t sizeEnderecoCliente = sizeof(enderecoCLiente);
    std::signal(SIGINT, manipulando_sinal_sistema_operacional);
    return new ClienteSocket(new Descritor(
            accept(descritor.paraPrimitivoInt(), &cast_socketaddr(enderecoCLiente), &sizeEnderecoCliente)));
}

void ServerSocket::iniciarServidor() {
    conectado = bind(descritor.paraPrimitivoInt(), &cast_socketaddr(endereco), sizeof(endereco)) >= 0;
    conectado = conectado && (listen(descritor.paraPrimitivoInt(), MAXIMA_QUANTIDADE_DE_CONEXOES) >= 0);
}

sockaddr &ServerSocket::cast_socketaddr(sockaddr_in &sockaddrIn) {
    return *(struct sockaddr *) (&sockaddrIn);
}

void ServerSocket::manipulando_sinal_sistema_operacional(int sinal) {
    throw std::runtime_error(std::to_string(sinal));
}

void ServerSocket::desconectarServidor() {
    conectado = false;
}
