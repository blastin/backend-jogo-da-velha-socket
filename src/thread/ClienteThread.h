//
// Created by jefferson on 15/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_CLIENTETHREAD_H
#define BACKEND_SOCKET_JOGODAVELHA_CLIENTETHREAD_H


#include "../cliente/ICliente.h"
#include "../sala/ISalaCliente.h"
#include "../cliente/IClienteComunicacao.h"
#include "../mensagem/ProtocoloConstrutor.h"

class ClienteThread {
private:
    IClienteComunicacao *cliente;
    ISalaCliente *salaCliente;
    ProtocoloConstrutor protocoloConstrutor;

    void comunicacaoComCliente();

    void comunicarCasoClienteNaoAutorizado();

    void comunicarCasoClienteEnvieProtocoloInvalido();

    void comunicarCasoOutroClienteNaoRecebeuMensagem();

    void comunicarCasoOutroClienteRecebeuMensagemComSucesso();

public:
    explicit ClienteThread(IClienteComunicacao *cliente, ISalaCliente *salaCliente) __attribute__((unused));

    ~ClienteThread();

    void iniciar();
};


#endif //BACKEND_SOCKET_JOGODAVELHA_CLIENTETHREAD_H
