//
// Created by jefferson on 15/09/18.
//

#include <iostream>
#include <thread>
#include <chrono>
#include "ClienteThread.h"
#include "../mensagem/MensagemComum.h"
#include "../mensagem/ProtocoloConstrutor.h"
#include "../mensagem/ProtocoloComunicativo.h"

__attribute__((unused)) ClienteThread::ClienteThread(IClienteComunicacao *cliente, ISalaCliente *salaCliente) : cliente(
        cliente), salaCliente(salaCliente) {}

ClienteThread::~ClienteThread() {

    salaCliente->removerCliente(cliente);

    delete cliente;

}

void ClienteThread::iniciar() {

    if (cliente->conectado()) {

        auto mensagemInicial = protocoloConstrutor.criarProtocoloPadraoEnvio(
                salaCliente->autorizadoAFazerComunicacao(cliente));

        cliente->enviarMensagem(&mensagemInicial);

        comunicacaoComCliente();

    }

}

void ClienteThread::comunicacaoComCliente() {

    while (cliente->conectado()) {

        IMensagem *mensagem = cliente->receberMensagem();

        if (cliente->recebimentoDeMensagemRealizadoComSucesso() and salaCliente->autorizadoAFazerComunicacao(cliente)) {

            ProtocoloComunicativo comunicativo(mensagem);

            if (comunicativo.mensagemValida()) {

                salaCliente->enviarMensagemParaOutroCliente(&comunicativo, cliente);

                if (salaCliente->mensagemAOutroClienteEnviadoComSucesso()) {

                    comunicarCasoOutroClienteRecebeuMensagemComSucesso();

                } else {

                    comunicarCasoOutroClienteNaoRecebeuMensagem();

                }

            } else {

                comunicarCasoClienteEnvieProtocoloInvalido();

            }

        } else {

            comunicarCasoClienteNaoAutorizado();

        }

        delete mensagem;

    }
}

void ClienteThread::comunicarCasoClienteNaoAutorizado() {

    if (cliente->conectado()) {

        auto mensagemProtocolo = protocoloConstrutor.criarProtocoloCasoClienteNaoAutorizado();

        cliente->enviarMensagem(&mensagemProtocolo);

    }
}

void ClienteThread::comunicarCasoClienteEnvieProtocoloInvalido() {

    if (cliente->conectado()) {

        auto mensagemProtocolo = protocoloConstrutor.criarProtocoloCasoClienteEnvieProtocoloInvalido();

        cliente->enviarMensagem(&mensagemProtocolo);

    }
}

void ClienteThread::comunicarCasoOutroClienteNaoRecebeuMensagem() {

    if (cliente->conectado()) {

        auto mensagemProtocolo = protocoloConstrutor.criarProtocoloCasoOutroClienteNaoRecebeuMensagem();

        cliente->enviarMensagem(&mensagemProtocolo);

    }
}

void ClienteThread::comunicarCasoOutroClienteRecebeuMensagemComSucesso() {

    if (cliente->conectado()) {

        auto mensagemProtocolo = protocoloConstrutor.criarProtocoloCasoOutroClienteRecebeuMensagemComSucesso();

        cliente->enviarMensagem(&mensagemProtocolo);

    }

}
