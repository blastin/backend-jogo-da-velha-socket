//
// Created by jefferson on 15/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_PORTA_H
#define BACKEND_SOCKET_JOGODAVELHA_PORTA_H


#include <netinet/in.h>

class Porta {

private:
    const in_port_t valor;
public:
    explicit Porta(in_port_t porta);
    in_port_t operator()() const;
};


#endif //BACKEND_SOCKET_JOGODAVELHA_PORTA_H
