//
// Created by jefferson on 15/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_DESCRITOR_H
#define BACKEND_SOCKET_JOGODAVELHA_DESCRITOR_H


#include "Identificador.h"

class Descritor : public Identificador {
private:
    const int valor;
public:
    explicit Descritor(int descritor);

    int paraPrimitivoInt() override;
};


#endif //BACKEND_SOCKET_JOGODAVELHA_DESCRITOR_H
