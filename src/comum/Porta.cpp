//
// Created by jefferson on 15/09/18.
//

#include "Porta.h"

in_port_t Porta::operator()() const {
    return valor;
}

Porta::Porta(const in_port_t porta) : valor(porta) {}

