//
// Created by jefferson on 23/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_IDENTIFICADOR_H
#define BACKEND_SOCKET_JOGODAVELHA_IDENTIFICADOR_H


class Identificador {

public:
    virtual ~Identificador() = default;

    virtual int paraPrimitivoInt() __attribute__((unused)) = 0;
};


#endif //BACKEND_SOCKET_JOGODAVELHA_IDENTIFICADOR_H
