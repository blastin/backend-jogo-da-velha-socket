#include <utility>

//
// Created by jefferson on 15/09/18.
//
#include <csignal>
#include <unistd.h>
#include <iostream>
#include "ClienteSocket.h"
#include "../mensagem/MensagemComum.h"

bool ClienteSocket::conectado() {
    return identificador->paraPrimitivoInt() > 0 and statusConectado;
}

IMensagem *ClienteSocket::receberMensagem() {

    char buffer[TAMANHO_BUFFER_RECEBIMENTO];

    ssize_t bytes = read(identificador->paraPrimitivoInt(), buffer, TAMANHO_BUFFER_RECEBIMENTO);

    resolveStatusMensagemRecebidaPorCliente(bytes);

    if (recebimentoDeMensagemRealizadoComSucesso()) {

        std::cout << "CLIENTE::D[" << identificador->paraPrimitivoInt() << "] recebendo mensagem" << std::endl;

        buffer[bytes] = '\0';

        return new MensagemComum(buffer);

    } else {
        return nullptr;
    }
}

ClienteSocket::ClienteSocket(Identificador *identificador) : Cliente(identificador) {
    comunicacaoEnvioMensagemSucesso = false;
    comunicacaoRecebimentoMensagemSucesso = false;
    statusConectado = true;
    std::cout << "CLIENTE::D[" << identificador->paraPrimitivoInt() << "] conectou" << std::endl;
}

ClienteSocket::~ClienteSocket() {
    close(identificador->paraPrimitivoInt());
    std::cout << "CLIENTE::D[" << identificador->paraPrimitivoInt() << "] desconectou" << std::endl;
}

bool ClienteSocket::envioDeMensagemRealizadoComSucesso() {
    return comunicacaoEnvioMensagemSucesso;
}

bool ClienteSocket::recebimentoDeMensagemRealizadoComSucesso() {
    return comunicacaoRecebimentoMensagemSucesso;
}

void ClienteSocket::resolveStatusMensagemRecebidaPorCliente(ssize_t bytes) {
    comunicacaoRecebimentoMensagemSucesso = bytes > 0;
    statusConectado = bytes != 0;
}

void ClienteSocket::enviarMensagem(IMensagem *mensagem) {
    try {
        std::signal(SIGPIPE, manipulando_sinal_sistema_operacional);
        const auto string = mensagem->obterMensagemEmFormatoString();
        std::cout << "CLIENTE::D[" << identificador->paraPrimitivoInt() << "] enviando mensagem" << std::endl;
        resolveStatusMensagemEnviadaPorCliente(write(identificador->paraPrimitivoInt(), string.data(), string.size()));
    } catch (const std::exception &e) {
        statusConectado = false;
        comunicacaoEnvioMensagemSucesso = false;
    };
}

void ClienteSocket::resolveStatusMensagemEnviadaPorCliente(ssize_t bytes) {
    comunicacaoEnvioMensagemSucesso = bytes > 0;
    statusConectado = bytes != 0;
}

void ClienteSocket::manipulando_sinal_sistema_operacional(int sinal) {
    throw std::runtime_error(std::to_string(sinal));
}
