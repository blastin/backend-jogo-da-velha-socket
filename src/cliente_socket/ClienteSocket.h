//
// Created by jefferson on 15/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_CLIENTESOCKET_H
#define BACKEND_SOCKET_JOGODAVELHA_CLIENTESOCKET_H


#include "../comum/Porta.h"
#include "../comum/Descritor.h"
#include "../cliente/ICliente.h"
#include "../cliente/Cliente.h"

class ClienteSocket : public Cliente {

private:
    const static unsigned TAMANHO_BUFFER_RECEBIMENTO = 512;
    bool comunicacaoEnvioMensagemSucesso;
    bool comunicacaoRecebimentoMensagemSucesso;
    bool statusConectado;

    inline void resolveStatusMensagemRecebidaPorCliente(ssize_t bytes);

    inline void resolveStatusMensagemEnviadaPorCliente(ssize_t bytes);

    void static manipulando_sinal_sistema_operacional(int sinal);

public:
    explicit ClienteSocket(Identificador *identificador);

    ~ClienteSocket() override;

    bool conectado() override;

    void enviarMensagem(IMensagem *mensagem) override;

    IMensagem *receberMensagem() override;

    bool envioDeMensagemRealizadoComSucesso() override;

    bool recebimentoDeMensagemRealizadoComSucesso() override;
};


#endif //BACKEND_SOCKET_JOGODAVELHA_CLIENTESOCKET_H
