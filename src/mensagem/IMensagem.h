//
// Created by jefferson on 23/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_IMENSAGEM_H
#define BACKEND_SOCKET_JOGODAVELHA_IMENSAGEM_H

#include <string>

class IMensagem {

public:
    virtual ~IMensagem() = default;
    virtual std::string obterMensagemEmFormatoString() __attribute__((unused)) = 0;

};


#endif //BACKEND_SOCKET_JOGODAVELHA_IMENSAGEM_H
