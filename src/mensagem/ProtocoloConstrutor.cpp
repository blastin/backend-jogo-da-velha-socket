//
// Created by jefferson on 08/10/18.
//

#include "../json_include/json.hpp"
#include "ProtocoloConstrutor.h"
#include "ProtocoloComunicativo.h"
#include "MensagemComum.h"

using namespace nlohmann;

MensagemComum ProtocoloConstrutor::criarProtocoloCasoClienteNaoAutorizado() {

    json mensagemProtocolo;

    mensagemProtocolo["versão"] = "V1.0-08/10/2018-21:53";
    mensagemProtocolo["api"] = "SOCKET";
    mensagemProtocolo["status"] = CLIENTE_EM_ESPERA;
    mensagemProtocolo["camadaMensagem"] = "CLIENTE EM ESPERA";

    return MensagemComum(mensagemProtocolo.dump());

}

MensagemComum ProtocoloConstrutor::criarProtocoloCasoClienteEnvieProtocoloInvalido() {

    json mensagemProtocolo;

    mensagemProtocolo["versão"] = "V1.0-08/10/2018-21:53";
    mensagemProtocolo["api"] = "SOCKET";
    mensagemProtocolo["status"] = CLIENTE_ENVIOU_PROTOCOLO_INVALIDO;
    mensagemProtocolo["camadaMensagem"] = "CLIENTE ENVIOU PROTOCOLO INVALIDO";

    return MensagemComum(mensagemProtocolo.dump());
}

MensagemComum ProtocoloConstrutor::criarProtocoloPadraoEnvio(bool clienteIniciaComunicacao) {

    json mensagemProtocolo;

    mensagemProtocolo["versão"] = "V1.0-08/10/2018-21:53";
    mensagemProtocolo["api"] = "SOCKET";
    mensagemProtocolo["status"] = CLIENTE_ENVIANDO_PROTOCOLO_INICIAL;
    mensagemProtocolo["camadaMensagem"]["clienteIniciaComunicacao"] = clienteIniciaComunicacao;

    return MensagemComum(mensagemProtocolo.dump());
}

MensagemComum ProtocoloConstrutor::criarProtocoloCasoOutroClienteNaoRecebeuMensagem() {
    json mensagemProtocolo;

    mensagemProtocolo["versão"] = "V1.0-08/10/2018-21:53";
    mensagemProtocolo["api"] = "SOCKET";
    mensagemProtocolo["status"] = CLIENTE_SALA_NAO_LOTADA;
    mensagemProtocolo["camadaMensagem"] = "SALA NÃO POSSUI OUTRO CLIENTE";

    return MensagemComum(mensagemProtocolo.dump());
}

MensagemComum ProtocoloConstrutor::criarProtocoloCasoOutroClienteRecebeuMensagemComSucesso() {
    json mensagemProtocolo;

    mensagemProtocolo["versão"] = "V1.0-08/10/2018-21:53";
    mensagemProtocolo["api"] = "SOCKET";
    mensagemProtocolo["status"] = CLIENTE_ENVIO_COM_SUCESSO;
    mensagemProtocolo["camadaMensagem"] = "MENSAGEM ENVIADA COM SUCESSO A OUTRO CLIENTE";

    return MensagemComum(mensagemProtocolo.dump());
}
