//
// Created by jefferson on 08/10/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_PROTOCOLOCOMUNICATIVO_H
#define BACKEND_SOCKET_JOGODAVELHA_PROTOCOLOCOMUNICATIVO_H


#include "IMensagem.h"
#include "../comum/Protocolos.h"

class ProtocoloComunicativo : public IMensagem {
private:
    IMensagem *camadaMensagemOriginal;
    std::string mensagemResposta;
    bool valido;
public:
    explicit ProtocoloComunicativo(IMensagem *mensagem);

    bool mensagemValida();

    std::string obterMensagemEmFormatoString() override;
};

#endif //BACKEND_SOCKET_JOGODAVELHA_PROTOCOLOCOMUNICATIVO_H
