//
// Created by jefferson on 08/10/18.
//

#include <iostream>
#include "ProtocoloComunicativo.h"
#include "../json_include/json.hpp"

using namespace nlohmann;


std::string ProtocoloComunicativo::obterMensagemEmFormatoString() {
    return mensagemResposta;
}

ProtocoloComunicativo::ProtocoloComunicativo(IMensagem *mensagem) : camadaMensagemOriginal(mensagem) {

    try {

        auto json = json::parse(camadaMensagemOriginal->obterMensagemEmFormatoString());

        valido = json.count("versão") == 1 and json.count("api") == 1 and
                 json.count("camadaMensagem") == 1;

        mensagemResposta = json.dump();

    } catch (const nlohmann::detail::parse_error &e) {
        valido = false;
    }
}

bool ProtocoloComunicativo::mensagemValida() {
    return valido;
}
