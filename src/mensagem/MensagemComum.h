//
// Created by jefferson on 23/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_MENSAGEMCOMUM_H
#define BACKEND_SOCKET_JOGODAVELHA_MENSAGEMCOMUM_H


#include "IMensagem.h"

class MensagemComum : public IMensagem {

private:
    std::string mensagem;
public:
    explicit MensagemComum(std::string mensagem);
    ~MensagemComum() override;
    std::string obterMensagemEmFormatoString() override;

};


#endif //BACKEND_SOCKET_JOGODAVELHA_MENSAGEMCOMUM_H
