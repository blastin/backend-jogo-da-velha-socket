#include <utility>

//
// Created by jefferson on 23/09/18.
//

#include "MensagemComum.h"

MensagemComum::MensagemComum(std::string mensagem) : mensagem(std::move(mensagem)){}

MensagemComum::~MensagemComum() = default;

std::string MensagemComum::obterMensagemEmFormatoString() {
    return mensagem;
}
