//
// Created by jefferson on 08/10/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_PROTOCOLOCONSTRUTOR_H
#define BACKEND_SOCKET_JOGODAVELHA_PROTOCOLOCONSTRUTOR_H

#include "MensagemComum.h"

class ProtocoloConstrutor {
public:

    MensagemComum criarProtocoloCasoClienteNaoAutorizado();

    MensagemComum criarProtocoloCasoClienteEnvieProtocoloInvalido();

    MensagemComum criarProtocoloPadraoEnvio(bool clienteIniciaComunicacao);

    MensagemComum criarProtocoloCasoOutroClienteNaoRecebeuMensagem();

    MensagemComum criarProtocoloCasoOutroClienteRecebeuMensagemComSucesso();

};


#endif //BACKEND_SOCKET_JOGODAVELHA_PROTOCOLOCONSTRUTOR_H
