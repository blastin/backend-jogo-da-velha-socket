#include <iostream>
#include <thread>
#include "server/ServerSocket.h"
#include "sala/Salas.h"
#include "thread/ClienteThread.h"

void iniciarConexoes(ServerSocket &serverSocket);

int main(int argc, char *argv[]) {

    Porta porta(3001);

    ServerSocket serverSocket(porta);
    serverSocket.iniciarServidor();

    if (serverSocket.servidorConectado()) {

        std::cout << "Servidor::Conectado" << std::endl;
        iniciarConexoes(serverSocket);

    }

    return 0;

}

void iniciarConexoes(ServerSocket &serverSocket) {

    Salas salas;

    do {

        try {

            auto cliente = serverSocket.novoCLienteSocket();

            if (cliente->conectado()) {

                auto sala = salas.inserirCliente(cliente);

                std::thread(&ClienteThread::iniciar,
                            move(std::make_unique<ClienteThread>(cliente, sala))).detach();

            }

        } catch (const std::exception &e) {
            serverSocket.desconectarServidor();
        };

    } while (serverSocket.servidorConectado());
}