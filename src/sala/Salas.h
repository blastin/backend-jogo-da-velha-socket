//
// Created by jefferson on 15/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_SALAS_H
#define BACKEND_SOCKET_JOGODAVELHA_SALAS_H

#include <list>
#include "ISalas.h"
#include "ISala.h"
#include "../cliente/ICliente.h"
#include "Sala.h"
#include "SalaID.h"

class Salas : public ISalas{
private:
    std::list<ISala*> lista;
    unsigned contadorDeSalas;
public:
    explicit Salas();
    ~Salas() override;
    ISala* inserirCliente(ICliente *cliente) override;
};


#endif //BACKEND_SOCKET_JOGODAVELHA_SALAS_H
