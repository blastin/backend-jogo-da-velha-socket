//
// Created by jefferson on 15/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_SALAID_H
#define BACKEND_SOCKET_JOGODAVELHA_SALAID_H


#include "../comum/Identificador.h"

class SalaID : public Identificador {
private:
    const int id;
public:
    explicit SalaID(int id);
    int paraPrimitivoInt() override;
};

#endif //BACKEND_SOCKET_JOGODAVELHA_SALAID_H
