//
// Created by jefferson on 15/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_ISALACLIENTE_H
#define BACKEND_SOCKET_JOGODAVELHA_ISALACLIENTE_H

#include "../cliente/ICliente.h"
#include <stdexcept>

class ISalaCliente {

public:

    virtual ~ISalaCliente() = default;

    virtual void enviarMensagemParaOutroCliente(IMensagem *mensagem, ICliente *cliente) __attribute__((unused)) = 0;

    virtual void removerCliente(ICliente *cliente) __attribute__((unused)) = 0;

    virtual bool salaLotada() __attribute__((unused)) = 0;

    virtual bool salaVazia() __attribute__((unused)) = 0;

    virtual int obterIdentificacaoSalaEmPrimitivo() __attribute__((unused)) = 0;

    virtual bool autorizadoAFazerComunicacao(ICliente* cliente)  __attribute__((unused)) = 0;

    virtual bool mensagemAOutroClienteEnviadoComSucesso() __attribute__((unused)) = 0;
};


#endif //BACKEND_SOCKET_JOGODAVELHA_ISALACLIENTE_H
