//
// Created by jefferson on 15/09/18.
//

#include <iostream>
#include "Salas.h"

Salas::Salas() {
    contadorDeSalas = 0;
}

Salas::~Salas() {

    std::cout << "Deletando salas" << std::endl;

    for (auto sala : lista) {
        delete sala;
    }

}

ISala *Salas::inserirCliente(ICliente *cliente) {

    for (auto sala : lista) {
        if (!sala->salaLotada()) {
            sala->inserirCliente(cliente);
            return sala;
        }
    }

    auto sala = new Sala(new SalaID(++contadorDeSalas));
    sala->inserirCliente(cliente);
    lista.push_back(sala);
    return sala;

}
