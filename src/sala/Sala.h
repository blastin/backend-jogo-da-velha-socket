//
// Created by jefferson on 15/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_SALA_H
#define BACKEND_SOCKET_JOGODAVELHA_SALA_H

#include <mutex>
#include <atomic>
#include "ISala.h"
#include "ISalas.h"

class Sala : public ISala {
private:
    ICliente *clienteA;
    ICliente *clienteB;
    ICliente *clienteComunicador;
    Identificador *salaID;
    std::mutex protecao;

    bool mensagemEnviadaComSucesso;

    unsigned clienteValorInicial: 1;

    int const VALOR_CLIENTE_A = 0;
    int const VALOR_CLIENTE_B = 1;

    void calcularClienteDeComunicacaoInicial();
    void trocarClienteHabilitadoComunicacao(ICliente* cliente);

public:
    explicit Sala(Identificador *salaID);

    ~Sala() override;

    void enviarMensagemParaOutroCliente(IMensagem *mensagem, ICliente *cliente) override;

    void inserirCliente(ICliente *cliente) override;

    bool salaLotada() override;

    void removerCliente(ICliente *cliente) override;

    bool salaVazia() override;

    int obterIdentificacaoSalaEmPrimitivo() override;

    bool autorizadoAFazerComunicacao(ICliente *cliente) override;

    bool mensagemAOutroClienteEnviadoComSucesso() override;

};


#endif //BACKEND_SOCKET_JOGODAVELHA_SALA_H
