//
// Created by jefferson on 15/09/18.
//

#include <iostream>
#include <random>
#include "Sala.h"

Sala::Sala(Identificador *salaID) : salaID(salaID), clienteA(nullptr), clienteB(nullptr), clienteValorInicial(0),
                                    clienteComunicador(nullptr), mensagemEnviadaComSucesso(false) {}

Sala::~Sala() {
    std::cout << "Removendo sala com ID(" << salaID->paraPrimitivoInt() << ")" << std::endl;
    delete salaID;
};

void Sala::enviarMensagemParaOutroCliente(IMensagem *mensagem, ICliente *cliente) {

    if (salaLotada() && mensagem != nullptr) {

        std::lock_guard<std::mutex> lock(protecao);

        if (cliente->identico(clienteA)) {

            clienteB->enviarMensagem(mensagem);

            mensagemEnviadaComSucesso = clienteB->envioDeMensagemRealizadoComSucesso();

            trocarClienteHabilitadoComunicacao(clienteB);

        } else if (cliente->identico(clienteB)) {

            clienteA->enviarMensagem(mensagem);

            mensagemEnviadaComSucesso = clienteA->envioDeMensagemRealizadoComSucesso();

            trocarClienteHabilitadoComunicacao(clienteA);

        }
    }
}

void Sala::inserirCliente(ICliente *cliente) {

    if (!salaLotada()) {

        std::lock_guard<std::mutex> lock(protecao);

        if (clienteA == nullptr && clienteB == nullptr) {
            calcularClienteDeComunicacaoInicial();
        }

        if (clienteA == nullptr) {

            clienteA = cliente;

            if (clienteValorInicial == VALOR_CLIENTE_A) {
                trocarClienteHabilitadoComunicacao(clienteA);
            }

        } else {

            clienteB = cliente;

            if (clienteValorInicial == VALOR_CLIENTE_B) {
                trocarClienteHabilitadoComunicacao(clienteB);
            }
        }

    }
}

bool Sala::salaLotada() {

    std::lock_guard<std::mutex> lock(protecao);

    return !(clienteA == nullptr || clienteB == nullptr);
}

void Sala::removerCliente(ICliente *cliente) {

    if (!salaVazia()) {

        std::lock_guard<std::mutex> lock(protecao);

        if (cliente->identico(clienteA)) {
            clienteA = nullptr;
        } else if (cliente->identico(clienteB)) {
            clienteB = nullptr;
        }
    }
}


bool Sala::salaVazia() {

    std::lock_guard<std::mutex> lock(protecao);

    return clienteA == nullptr && clienteB == nullptr;
}

int Sala::obterIdentificacaoSalaEmPrimitivo() {
    return salaID->paraPrimitivoInt();
}

bool Sala::autorizadoAFazerComunicacao(ICliente *cliente) {
    return cliente->identico(clienteComunicador);
}

void Sala::calcularClienteDeComunicacaoInicial() {

    std::cout << "Sala(" << salaID->paraPrimitivoInt() << ") calculando comunicação inicial" << std::endl;

    std::random_device random;
    std::mt19937 aleatorio(random());
    std::uniform_int_distribution<> distribuicao(VALOR_CLIENTE_A, VALOR_CLIENTE_B);
    clienteValorInicial = static_cast<unsigned int>(distribuicao(aleatorio));
}

void Sala::trocarClienteHabilitadoComunicacao(ICliente *cliente) {
    if (cliente->identico(clienteA) || cliente->identico(clienteB)) clienteComunicador = cliente;
}

bool Sala::mensagemAOutroClienteEnviadoComSucesso() {

    bool estadoAnterior = mensagemEnviadaComSucesso;

    mensagemEnviadaComSucesso = false;

    return estadoAnterior;
}
