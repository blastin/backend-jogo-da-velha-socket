//
// Created by jefferson on 15/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_ISALAS_H
#define BACKEND_SOCKET_JOGODAVELHA_ISALAS_H


#include "ISala.h"

class ISalas {
public:
    virtual ~ISalas() = default;
    virtual ISala* inserirCliente(ICliente *cliente) __attribute__((unused)) = 0;
};


#endif //BACKEND_SOCKET_JOGODAVELHA_ISALAS_H
