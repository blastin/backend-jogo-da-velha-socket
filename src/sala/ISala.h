//
// Created by jefferson on 15/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_ISALA_H
#define BACKEND_SOCKET_JOGODAVELHA_ISALA_H

#include <stdexcept>
#include "ISalaCliente.h"
#include "../cliente/ICliente.h"

class ISala : public ISalaCliente {

public:

    ~ISala() override = default;

    virtual void inserirCliente(ICliente * cliente) __attribute__((unused)) = 0;
};


#endif //BACKEND_SOCKET_JOGODAVELHA_ISALA_H
