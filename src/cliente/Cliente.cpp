//
// Created by jefferson on 23/09/18.
//
#include <iostream>
#include "Cliente.h"

Cliente::Cliente(Identificador *identificador) : identificador(identificador) {}

Cliente::~Cliente() {
    std::cout << "Deletando Identificador(" << identificador->paraPrimitivoInt() << ") Cliente." << std::endl;
    delete identificador;
}

bool Cliente::identico(ICliente *cliente) {
    return cliente == this;
}
