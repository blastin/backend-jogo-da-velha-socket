//
// Created by jefferson on 23/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_ICLIENTECOMUNICACAO_H
#define BACKEND_SOCKET_JOGODAVELHA_ICLIENTECOMUNICACAO_H


#include "ICliente.h"

class IClienteComunicacao : public ICliente {

public:

    ~IClienteComunicacao() override = default;

    virtual IMensagem *receberMensagem() __attribute__((unused)) = 0;

    virtual bool recebimentoDeMensagemRealizadoComSucesso() __attribute__((unused)) = 0;
};


#endif //BACKEND_SOCKET_JOGODAVELHA_ICLIENTECOMUNICACAO_H
