//
// Created by jefferson on 15/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_ICLIENTE_H
#define BACKEND_SOCKET_JOGODAVELHA_ICLIENTE_H


#include <string>
#include "../comum/Identificador.h"
#include "../mensagem/IMensagem.h"

class ICliente {


public:
    virtual ~ICliente() = default;

    virtual bool conectado() __attribute__((unused)) = 0;

    virtual void enviarMensagem(IMensagem * mensagem) __attribute__((unused)) = 0;

    virtual bool envioDeMensagemRealizadoComSucesso() __attribute__((unused)) = 0;

    virtual bool identico(ICliente *cliente) __attribute__((unused)) = 0;

};


#endif //BACKEND_SOCKET_JOGODAVELHA_ICLIENTE_H
