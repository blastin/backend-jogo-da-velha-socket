//
// Created by jefferson on 23/09/18.
//

#ifndef BACKEND_SOCKET_JOGODAVELHA_CLIENTE_H
#define BACKEND_SOCKET_JOGODAVELHA_CLIENTE_H


#include "IClienteComunicacao.h"

class Cliente : public IClienteComunicacao {

protected:

    Identificador *identificador;

public:

    explicit Cliente(Identificador *identificador);

    ~Cliente() override;

    bool identico(ICliente *cliente) override;

};


#endif //BACKEND_SOCKET_JOGODAVELHA_CLIENTE_H
